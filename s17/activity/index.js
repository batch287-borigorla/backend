/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function firstfunc() {
		let fullname=prompt("Enter your full name");
		let age=prompt("Enter your age");
		let location=prompt("Enter your location");

		console.log("Hello, "+fullname);
		console.log("You are "+age +" years old.");
		console.log("You live in "+location);
	};

	firstfunc();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favmusi() {
		let mus1="Arijit Singh";
		let mus2="Armaan Malik";
		let mus3="Shreya Ghoshal";
		let mus4="Jubin Nautiyal";
		let mus5="Darshan Raval";

		console.log("1. "+mus1);
		console.log("2. "+mus2);
		console.log("3. "+mus3);
		console.log("4. "+mus4);
		console.log("5. "+mus5);
	};
	favmusi();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favmov() {
		let favmov1="Forensic";
		let rat1="71%";
		let favmov2="Midnight Murders";
		let rat2="65%";
		let favmov3="Hridayam";
		let rat3="56%";
		let favmov4="Final Destination";
		let rat4="67%";
		let favmov5="Interstellar";
		let rat5="73%";

		console.log("1. " + favmov1);
		console.log("Rotten Tomatoes rating : " + rat1);
		console.log("2. " + favmov2);
		console.log("Rotten Tomatoes rating : " + rat2);
		console.log("3. " + favmov3);
		console.log("Rotten Tomatoes rating :" + rat3);
		console.log("4. " + favmov4);
		console.log("Rotten Tomatoes rating :" + rat4);
		console.log("5. " + favmov5);
		console.log("Rotten Tomatoes rating :" + rat5);


			};
			favmov();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

