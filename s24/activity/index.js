console.log("Hi!!!");

const getCube=2**3
console.log(`The cube of 2 is ${getCube}`);

const address=[258,'Washington Ave NW','California',90011];

console.log(`I live at ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`);

const animal={
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: '1075 kgs',
	measurement: '20 ft 3 in'
};

console.log(`${animal.name} was a ${animal.type}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}.`);

let num=[1,2,3,4,5];

num.forEach((num) => {
	console.log(`${num}`);
});

let reduceNumber=num.reduce((accumulator,currentValue)=> 
	accumulator+currentValue
);

console.log(reduceNumber);

class Dog {
	constructor(name,age,breed) {
		this.name=name;
		this.age=age;
		this.breed=breed;
	}
}

let myDog=new Dog("Frankie",5,"Miniature Dachshund");
console.log(myDog);




