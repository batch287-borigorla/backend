const express = require("express");

const app = express();

const port = 3000;

app.use(express.json())

app.use(express.urlencoded({extended:true}));

app.get("/home", (request, response) => {
		response.send('Welcome to the home page');
	})

let users = [{
	username: "johndoe",
	password: "johndoe1234"
}];  //This is our mockdatabase


app.get("/users", (req,res)=>{
	res.send(users);
})

app.delete("/delete-item",(req,res)=>{
	for(let i=0;i<users.length;i++){
		if(req.body.username===users[i].username){
			users.pop();
			res.send(`User ${req.body.username} has been deleted`);
			console.log(users);
			break;
		}
		else res.send("User does not exist.");
	}
})


app.listen(port, () => console.log(`Server is currently running at port ${port}`));