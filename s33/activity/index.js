console.log("hi");

fetch("https://jsonplaceholder.typicode.com/todos", {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
	},
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos", {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
	},
})
.then((response) => response.json())
.then((json) => {
	let titles=json.map(list=>list.title);
	console.log(titles);
});

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'GET',
		headers: {
			'content-Type' : 'application/json'		
		},
	})
	.then((response) => response.json())
	.then((json) => {console.log(json);
	console.log(`The item "${json.title}" on the list has a status of ${json.completed}.`)});

fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'content-Type' : 'application/json'		
		},
		body: JSON.stringify({
			title: "Created To Do List Item",
			completed: false,
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json) );

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
			'content-Type' : 'application/json'		},
		body: JSON.stringify({
			title: "Updated To Do List Item",
			description: "To update the my to do list with a different data structure" ,
			status: "pending" ,
			dateCompleted: "pending",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json) );

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		headers: {
			'content-Type' : 'application/json'		},
		body: JSON.stringify({
			status: "Complete" ,
			dateCompleted: "06/07/23",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json) );

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'DELETE'
	})



